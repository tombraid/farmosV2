echo '\n\n'
echo '    ______                                   _    _____ 
   / ____/___ __________ ___  ____  _____   | |  / /__ \
  / /_  / __ `/ ___/ __ `__ \/ __ \/ ___/   | | / /__/ /
 / __/ / /_/ / /  / / / / / / /_/ (__  )    | |/ // __/ 
/_/    \__,_/_/  /_/ /_/ /_/\____/____/     |___//____/ 
                                                        '

dirpath=`dirname $0`
echo $dirpath 
cd $dirpath
SHELL_PATH=`pwd -P`

echo '\n\n 1. apt update\n';sudo apt update
echo '\n\n 2. apt install -y build-essential\n';sudo apt install -y build-essential
echo '\n\n 3. apt install curl\n'; sudo apt install -y curl

echo '\n\n 4. apt install python-pip\n'; sudo apt install -y python-pip
echo '\n\n 4-1. pip install requests\n'; pip install requests
echo '\n\n 4-2. pip install pypaho-mqtt\n'; pip install paho-mqtt
echo '\n\n 4-3. pip install pymysql\n'; pip install pymysql
echo '\n\n 4-4. pip install pymodbus\n'; pip install pymodbus
echo '\n\n 4-5. pip install subprocess32\n'; pip install subprocess32
echo '\n\n 4-6. pip install simpleeval\n'; pip install simpleeval


echo '\n\n 5. mysql check\n'
which mysql
if [ $? -eq 1 ];then
   echo "\n\n apt install mysql-server\n"; sudo apt install -y mysql-server 
   echo "\n\n systemctl start mysql\n"; sudo systemctl start mysql
   echo "\n\n systemctl enable mysql\n"; sudo systemctl enable mysql
else
    echo "mysql installed"
fi
echo "\nend"

echo '\n\n 6. node check\n'
which node
if [ $? -eq 1 ];then
    echo "apt purge node\n"; sudo apt purge node
fi


echo "curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -\n"; curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -;
echo "apt install -y nodejs\n"; sudo apt install -y nodejs
echo "npm install forever -g\n"; npm install forever -g
echo "\nend"

echo '\n\n 7. Mosquitto check\n'
which mosquitto
if [ $? -eq 1 ];then
   echo "\n\n apt install mosquitto\n"; sudo apt install -y mosquitto 
   echo "\nport 1883\nprotocol mqtt\n\nlistener 9001\nprotocol websockets" | sudo tee -a /etc/mosquitto/mosquitto.conf
   echo "\n\n sudo systemctl restart mosquitto\n"; sudo systemctl restart mosquitto
   sleep 2
   echo "\n\n sudo mosquitto -c /etc/mosquitto/mosquitto.conf -d\n"; sudo mosquitto -c /etc/mosquitto/mosquitto.conf -d
else
    echo "mosquitto installed"
fi
echo "\nend"

echo '\n\n 8. database script run \n'
sudo mysql -u root < farmos.sql

echo '\n\n 9. npm install \n'
npm --prefix ../server/modules/database.js install ../server/modules/database.js
npm --prefix ../server/api install ../server/api

echo '\n\n 10. ui installation \n'
cat << "EOF" > "fui"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          farmos_ui
# Required-Start:    mysql
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable farmos UI service provided by daemon.
### END INIT INFO
EOF

echo "WORK_DIR=\"${SHELL_PATH%/*}/server/api\"" >> fui

cat << "EOF" >> "fui"
case "$1" in
  start)
    echo "Starting server"
    cd "$WORK_DIR"
    forever start --uid "farmosV2" -a "${WORK_DIR}/app.js"
    ;;
  stop)
    echo "Stopping server"
    forever stop farmosV2
    ;;
  *)
    echo "Usage: /etc/init.d/fui {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x fui
sudo mv fui /etc/init.d/fui

echo '\n\n 11. gate installation \n'
cat << "EOF" > "cvtgate"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          farmos_gate
# Required-Start:    mysql
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable farmos gate service provided by daemon.
### END INIT INFO
EOF

echo "WORK_DIR=\"${SHELL_PATH%/*}/gate\"" >> cvtgate

cat << "EOF" >> "cvtgate"
case "$1" in
  start)
    echo "Starting server"
    cd "$WORK_DIR"
    python couplemng.py start
    ;;
  stop)
    echo "Stopping server"
    cd "$WORK_DIR"
    python couplemng.py stop
    ;;
  *)
    echo "Usage: /etc/init.d/cvtgate {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x cvtgate
sudo mv cvtgate /etc/init.d/cvtgate

echo '\n\n 12. core installation \n'
cat << "EOF" > "fcore"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          farmos_core
# Required-Start:    mysql
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable farmos core service provided by daemon.
### END INIT INFO
EOF

echo "WORK_DIR=\"${SHELL_PATH%/*}/fcore\"" >> fcore

cat << "EOF" >> "fcore"
case "$1" in
  start)
    echo "Starting server"
    cd "$WORK_DIR"
    python fcore.py start
    ;;
  stop)
    echo "Stopping server"
    python fcore.py stop
    ;;
  *)
    echo "Usage: /etc/init.d/fcore {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x fcore
sudo mv fcore /etc/init.d/fcore

echo '\n\n 13. service run\n'

sudo /etc/init.d/fui start
sudo /etc/init.d/fcore start
sudo /etc/init.d/cvtgate start